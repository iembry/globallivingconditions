<<setup.gis, echo=FALSE, tidy=FALSE, message=FALSE>>=
opts_chunk$set(results='hide', cache=TRUE, echo=TRUE, warning=TRUE, fig.pos = 'htb', 
tidy.opts=list(blank=FALSE, width.cutoff=50), background='white', crop=TRUE,
tidy=FALSE, error=TRUE, message=FALSE, dev="tikz", autodep = TRUE)
opts_knit$set(root.dir = ".")
knit_hooks$set(crop=hook_pdfcrop)
options(scipen=999) ## Avoid scientific formating of large numbers.
@ 

<<include.general.latex.stuff, echo=FALSE, cache=FALSE>>=
set_parent('~/src/dhsharmonisation/inst/latex-headers.Rnw')
@ 

<<set.a.global.constant>>=
library(sp)
lat.long.proj.str <- CRS(
    "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")
@ 

<<threshold>>=
threshold <- .1
@ 

Which are the African countries? GADM has a category Africa which we can use to find out. Admittely, it is overkill to get 

<<big.1>>=
big.1.f <- function() {
    library(rgdal)
    world <- readOGR(dsn="/home/hans/annex/projekt/child-poverty/statiska-eller-stora-filer/GADM/", layer="gadm28_adm0")
    return(as.character(world@data$NAME_ENGLI[(world@data$UNREGION2 == "Africa" & is.na(world@data$UNREGION2) == FALSE)]))
}
african.countries <- big.1.f()
@ 

For the spatial analysis of the point process, we concentrate on mainland africa, so must filter out all countries that are islands. Madagascar is a special case, we want war-intensity estimates for it, but we will make them separately from mainland Africa.

<<reduce>>=
reduce.f <- function() {
my.countries <- african.countries[which(african.countries %in% c("Seychelles", "Sao Tome and Principe", "Saint Helena", "Mauritius", "Reunion", "Cape Verde", "Comoros", "Mayotte") == FALSE)]
library(car)
my.countries <- recode(my.countries, recodes = "'Côte d\\'Ivoire' = 'Ivory Coast'")
library(rgdal)
library(maps)
library(maptools)
africa <- SpatialPolygons(lapply(sapply(my.countries, function(country) { 
    my.map <- map(database='world', country, plot = FALSE, fill=TRUE)
    my.sp <- map2SpatialPolygons(my.map,
                                 IDs=sapply(strsplit(my.map$names, ":"), function(x) x[1]),
                                 proj4string=CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))
    return(my.sp)
}), function(x){x@polygons[[1]]}))

## remove country borders
my.africa <- unionSpatialPolygons(polygons(africa),
                               rep(1, length(africa)))

madagascar <- SpatialPolygons(list(
                   Polygons(list(
                   my.africa@polygons[[1]]@Polygons[[6]]
                     ), 1)),
                   proj4string=CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))

mainland.africa <- SpatialPolygons(list(
                   Polygons(list(
                   my.africa@polygons[[1]]@Polygons[[22]]
                     ), 1)),
                   proj4string=CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))
return(list(mainland.africa, madagascar))
}
my.africa <- reduce.f()
@ 

%% Now that we have polygons for mainland Africa and Madagascar we can create subset of points for each time-window for which we want a war estimate. For this we use the class \texttt{ppp} from the package \texttt{spatstat}.

\section{Import UCDP Georeferenced Event Dataset Version 5.0}
\label{sec:import-ucdp-geor}

Some points in UCDP are slightly off from Africas coast (using borders from the GADM database of Global Administrative Areas). We solve this by enlarging africa slightly.

<<enlargen.africa>>=
library(sp)
enlargen.polygon.f <- function(my.sp, width = 50000, CRSobj=CRS("+init=epsg:32662")) {
    ## Enlarge Africa, so that points that are slightly off will be included
    ## Enlarging requires projection, we use equal distance, Plate Carree 
    ## see http://spatialreference.org/ref/epsg/32662/
    ## 32663 is supposed to be even better, but it is not available, CRS() returns 
    ## an error.
    projected.polygon <- spTransform(x=my.sp, CRSobj=CRSobj)
    ## enlarge
    library(rgeos)
    polygon.larger <- gBuffer(projected.polygon, width = width)
    ## transform back to WGS84
    return(spTransform(x=polygon.larger, 
      CRSobj = "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))
}
large.mainland.africa <- enlargen.polygon.f(my.sp = my.africa[[1]])
proj4string(my.africa[[2]]) <- proj4string(my.africa[[1]])
large.madagascar <- enlargen.polygon.f(my.sp = my.africa[[2]])
@ 

The following chunk imports the UCDP database, which covers armed conflicts back to 1989.

<<ucdp>>=
library(sp)
library(data.table)
wash.ucdp.f <- function(file, africa){
    load(file)
    ## Egypt is "Middle East" in UCDP
    these <- which(ged50@data$region == "Africa" | ged50@data$country == "Egypt")
    my.sp <- SpatialPointsDataFrame(
        coords = ged50@coords[these,],
        data = ged50@data[these, c("best_est", "date_start", "date_end")],
        proj4string = CRS(proj4string(ged50))
    )
    good.points <- over(my.sp, africa)
    ## length(which(is.na(good.points)))
    ## 11 points are truly off, return the other points
    sp.temp <- my.sp[which(is.na(good.points) == FALSE), ]
    ## expand multi-day records to one-day events START
    foo <- apply(sp.temp@data, 1, function(row) {
        dates <- seq(from = as.Date(row[['date_start']]), 
                     to = as.Date(row[['date_end']]), 
                     by = 1)
        fatalities <- rep(x=as.numeric(row[['best_est']])/length(dates), 
                          times=length(dates))
        return(data.table(date = dates,
                          fatalities))
    })
    bar <- rbindlist(foo, idcol="event.id")
    repeats <- apply(sp.temp@data, 1, function(row) {
        length(seq(from = as.Date(row[['date_start']]), 
                     to = as.Date(row[['date_end']]), 
                     by = 1))
    })
    lon <- rep(sp.temp@coords[ ,1], times = repeats)
    lat <- rep(sp.temp@coords[ ,2], times = repeats)
    ## expand multi-day records to one-day events STOP    
    return(SpatialPointsDataFrame(coords=list(lon, lat), 
                                  data=data.frame(
                                      EVENT_DATE = bar$date, 
                                      FATALITIES = bar$fatalities), 
                                  proj4string=CRS(proj4string(ged50))))
}
ucdp.mainland.spdf <- wash.ucdp.f(file = "~/annex/projekt/child-poverty/statiska-eller-stora-filer/UppsalaConflictDataProgramme/ged50.Rdata", africa = large.mainland.africa)
ucdp.madagascar.spdf <- wash.ucdp.f(file = "~/annex/projekt/child-poverty/statiska-eller-stora-filer/UppsalaConflictDataProgramme/ged50.Rdata", africa = large.madagascar)
@ 

We want to be able to compare UCDP with ACLED, so make a general algorithm that can be used by both of them, unlike the previous version of these functions, which relied on data in the global environment: my.dates and month.dt, this function requires those given as arguments.

<<event.types.full>>=
event.types.full = c("Battle-No change of territory",
                "Battle-Government regains territory",
                "Battle-Non-state actor overtakes territory",
                "Violence against civilians")
@ 

<<month.dt.f>>=
## suming monthly
library(data.table)
library(sp)
month.dt.f <- function(my.spdf) {
    full.dt <- data.table(LONGITUDE = my.spdf@coords[, 1],
                          LATITUDE = my.spdf@coords[, 2],
                          FATALITIES = my.spdf@data$FATALITIES,
                          DATE = my.spdf@data$EVENT_DATE,
                          year.month = zoo::as.yearmon(as.POSIXct(my.spdf@data$EVENT_DATE)))
    month.dt <- full.dt[ , 
                        sum(FATALITIES), 
                        by = c("LONGITUDE", "LATITUDE", "year.month")]
    return(month.dt)
}
ucdp.mainland.by.month <- month.dt.f(ucdp.mainland.spdf)

mid.months.points.f <- function(years) {
    as.Date(as.POSIXct(as.vector(sapply(years, function(year) { sapply(1:12,
      function(month) { as.POSIXct(zoo::as.yearmon(paste(year, 
                           month, sep = "-"))) + 3600 * 24 * 14 }) })), 
                           origin = "1970-01-01 00:00:00 CET")[-c(1:6)])    
}

check.points.ucdp <- mid.months.points.f(1989:2015)
library(spatstat)
library(maptools)
## make slightly larger owindow
large.owindow <- as(large.mainland.africa, "owin") ## Mainland Africa
window.f.new <- function(end.date, go.back.seconds, month.dt) {
    library(globallivingconditions)
    my.log.f(paste("class of end.date", class(end.date)), log.file="my.gis.log")    
    my.dates <- as.POSIXct(zoo::as.Date(month.dt$year.month))    
    my.log.f(paste("class of month.dt$year.month", class(month.dt$year.month)), log.file="my.gis.log")            
    my.log.f(paste("class of my.dates", class(my.dates)), log.file="my.gis.log")        
    these <- which(my.dates >= end.date-go.back.seconds &
                   my.dates < end.date)
    my.log.f(paste("length of these", length(these)), log.file="my.gis.log")            
    my.dt <- month.dt[these, sum(V1), by = c("LONGITUDE", "LATITUDE")]
    my.sp <- SpatialPoints(
        coords = list(x = my.dt$LONGITUDE,
                      y = my.dt$LATITUDE),
        proj4string=CRS(
     "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))
    return(list(ppp(x=my.sp@coords[ ,1], 
                    y=my.sp@coords[ ,2], 
                    marks = my.dt$V1, 
                    window=large.owindow),
                my.dt$V1))
}
find.non.zero.polygons.new <- function(end.date, go.back.seconds, threshold, month.dt){
    my.ppp.list <- window.f.new(end.date, go.back.seconds, month.dt)
    my.density <- density(my.ppp.list[[1]], sigma=1, weights=my.ppp.list[[2]])
    my.temp.spdf <- as(as.SpatialGridDataFrame.im(my.density), "SpatialPointsDataFrame")
    proj4string(my.temp.spdf) <- lat.long.proj.str
    these <- which(my.temp.spdf$v >= threshold)
    if(length(these) > 0){
        x <- my.temp.spdf@coords[these, 1];     y <- my.temp.spdf@coords[these, 2]; 
        x1 <- x-my.density$xstep/2; x2 <- x+my.density$xstep/2 ;
        y1 <- y-my.density$ystep/2; y2 <- y+my.density$ystep/2 ;    
        return(SpatialPolygonsDataFrame(Sr=
          SpatialPolygons(sapply(1:length(these), function(i){
            Polygons(
                list(Polygon(
                    rbind(cbind(x1[i], y1[i]), 
                          cbind(x1[i], y2[i]), 
                          cbind(x2[i], y2[i]), 
                          cbind(x2[i], y1[i]), 
                          cbind(x1[i], y1[i])))),
                ID=i)}),
            proj4string=lat.long.proj.str),
        data=data.frame(intensity=my.temp.spdf@data$v[these])))
    }
}
@ 

<<ucdp.with.general.algo>>=
library(data.table)
library(spatstat)
library(maptools)
a.half.year.in.seconds <- 3600 * 24 * 365.24/2 ## half a year (no leap year on
                                        ## even centuries, thus not 365.25)
war.intensity.ucdp.6.months <- parallel::mclapply(as.POSIXct(check.points.ucdp), 
             find.non.zero.polygons.new, go.back.seconds=a.half.year.in.seconds, 
             threshold=threshold, month.dt = ucdp.mainland.by.month, mc.cores=4)
names(war.intensity.ucdp.6.months) <- check.points.ucdp
war.intensity.ucdp.12.months <- parallel::mclapply(as.POSIXct(check.points.ucdp), 
             find.non.zero.polygons.new, go.back.seconds=2 * a.half.year.in.seconds,
             threshold=threshold, month.dt = ucdp.mainland.by.month, mc.cores=4)
names(war.intensity.ucdp.12.months) <- check.points.ucdp
war.intensity.ucdp.24.months <- parallel::mclapply(as.POSIXct(check.points.ucdp), 
             find.non.zero.polygons.new, go.back.seconds=4 * a.half.year.in.seconds,
             threshold=threshold, month.dt = ucdp.mainland.by.month, mc.cores=4)
names(war.intensity.ucdp.24.months) <- check.points.ucdp
@ 

<<output>>=
save(war.intensity.ucdp.6.months, war.intensity.ucdp.12.months, 
     war.intensity.ucdp.24.months, african.countries, 
     file = "intensity-of-armed-conflicts-Africa-UCDP.RData")
@ 

\end{document}
