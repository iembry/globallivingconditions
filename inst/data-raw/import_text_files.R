# import the text files to transform into .rda files

import::from(data.table, data.table)
import::from(rio, import)

# lenanthro
lenanthro <- import(system.file("lenanthro.txt", package = "globallivingconditions"), setclass = "data.table")

# save lenanthro
save(lenanthro, file = "./inst/lenanthro.rda")


# weianthro
weianthro <- import(system.file("weianthro.txt", package = "globallivingconditions"), setclass = "data.table")

# save weianthro
save(weianthro, file = "./inst/weianthro.rda")



# wflanthro
wflanthro <- import(system.file("wflanthro.txt", package = "globallivingconditions"), setclass = "data.table")

# save wflanthro
save(wflanthro, file = "./inst/wflanthro.rda")
