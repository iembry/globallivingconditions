# R script that saves the MICS Country Aliases into a rda file


import::from(httr2, request, req_perform, resp_body_string, "%>%")
import::from(mgsub, mgsub)
import::from(abjutils, rm_accent)
import::from(stringi, stri_enc_isascii)
import::from(jsonlite, fromJSON)
import::from(data.table, as.data.table, rbindlist, set, setcolorder, setorder, setnames, "%like%", ":=")

# Country Alias URL
url <- "https://mics.unicef.org/api/countryAliases"

# Create a new request & perform the request. Also return all content received.
get_response <- httr2::request(url) %>% req_perform(verbosity = 3)

# Return the response as a string
surveyAlias_json <- get_response %>% resp_body_string()

# get the resulting JSON information
surveyAlias <- jsonlite::fromJSON(surveyAlias_json)

# save as a data.table
surveyAliass <- as.data.table(surveyAlias)

# change the column names
setnames(surveyAliass, c("Country", "Survey_Names"))

survey_alias_read <- as.list(surveyAliass$Survey_Names)

# name the list elements of the Reports list
names(survey_alias_read)[1:length(survey_alias_read)] <- surveyAliass$Country

# bind the Survey Alias list elements using data.table
survey_alias <- rbindlist(survey_alias_read, idcol = TRUE, fill = TRUE)

# change the column names
setnames(survey_alias, c("Country", "Survey_Names", "Year"))

survey_alias[, Clean_Country := Country]



# remove accents from Country names
column_choosedrr <- "Clean_Country"

# Source 5 r - data.table set add number to certain j values only - Stack Overflow answered by chinsoon12 on May 16 2018. https://stackoverflow.com/questions/50361168/r-data-table-set-add-number-to-certain-j-values-only
# Source 5 begin
for (col in column_choosedrr) {
idx1 <- which(!stri_enc_isascii(survey_alias[[col]]))
set(survey_alias, i = idx1, j = col, value = rm_accent(survey_alias[[col]][idx1]))
}


# remove , (, ), and ' from Country names
for (col in column_choosedrr) {
idx2 <- which(survey_alias$Country %like% ",|\\(|\\)|'")
set(survey_alias, i = idx2, j = col, value = mgsub(survey_alias[[col]][idx2], pattern = ",|\\(|\\)|'", replacement = ""))
}


# replace a single space with the underscore in Country names
for (col in column_choosedrr)
set(survey_alias, j = col, value = mgsub(survey_alias[[col]], pattern = " ", replacement = "_"))
# Source 5 end

setcolorder(survey_alias, c("Country", "Clean_Country", "Survey_Names", "Year"))

setorder(survey_alias, Country)


## the MICS country aliases are saved within the "inst" folder
save(survey_alias, file = "./inst/MICS_country_aliases.rda")
