my.log.f <- function(my.string, log.filename = "living-conditions.log"){
    ## if log.filename is an empty string, then use the default
    if(log.filename == ""){ log.filename = "living-conditions.log" }
    my.log <- file(description = log.filename, open = "at", blocking = FALSE)
    writeLines(my.string, con = my.log)
    close(my.log)
}
