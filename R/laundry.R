## Harmonize the spelling, where needed
.simpleCap.vec <- function(y){
    sapply(y, .simpleCap)
}
.simpleCap <- function(x) {
    s <- strsplit(x, " ")[[1]]
    paste(toupper(substring(s, 1, 1)), substring(s, 2),
          sep = "", collapse = " ")
}

add.missing.district.names <- function(source, RegionID, district.code){
    load(file = system.file("district_names.RData", 
               package="globallivingconditions")) ## district.names
    foo.temp <- data.table(source, RegionID, district.code)
    setkey(foo.temp, source, RegionID, district.code)    
    foo <- unique(foo.temp)
    setkey(foo, source, RegionID, district.code)
    bar <- merge(x=foo, y=district.names, all.x=TRUE, all.y=FALSE)
    bal <- unique(bar)
    setkey(bal, source, RegionID, district.code)
    baz <- merge(x=foo.temp, y=bal, all.x=TRUE, all.y=FALSE)    
    return(baz$District)
}

## wash variables with attribute value.labels,
## convert into factors, or plain numbers

## This function (numeric.from.named.atomic) numeric.from.named.atomic()
## touches only encoded values which have
## a "value.label". It will (silently) ignore purely numerical values. Another way
## of expressing the same thing special.recodes must be a character vector.

raw.from.named.atomic <- function(my.df, variable.name, s=NULL,
              special.recodes=NULL){
    return(my.df[[variable.name]]) }

numeric.from.named.atomic <- function(my.df, variable.name, s=NULL,
              special.recodes=NULL){
    ## If s is given, it should be a list with an element "standard.recodes"
    ## which must a be string. data.laundry.input works as s.

    ## if the variable is missing, return NA
    if(is.na(match(variable.name, names(my.df)))){
        return(rep(NA, times = nrow(my.df)))
    }
    ## if the variable exists but holds only missing data return NA (strip meta data)
    if(length(which(is.na(my.df[[variable.name]]))) == nrow(my.df)){
        return(rep(NA, times = nrow(my.df)))
    }

    ## apply the metadata specifications, if any
    these.are.special <- which(my.df[[variable.name]] %in%
                               attr(my.df[[variable.name]], "value.labels"))
    if(length(these.are.special) > 0){
        ## classify according to the standard recodes, and
        ## the special recodes, if any.
        if(is.null(s) == FALSE){
            if(is.null(special.recodes) == FALSE && special.recodes != ""){
                ## both standard and special recodes
                recodes <- paste(s$standard.recodes, special.recodes, sep = ";")
            } else {
                ## only standard recodes
                recodes <- s$standard.recodes
            }
        } else {
            if(is.null(special.recodes) || special.recodes == ""){
                ## Assume all numeric values without labels are good and that
                ## all labels only specify different causes of missing data, 
                ## e.g. line number of mother HC60.
                ## my.log.f(paste0("Assuming value lable presence indicates NA",
                ##      " for variable ", variable.name, " with value label: ", 
                ##      names(attr(my.df[[variable.name]], "value.labels")),
                ##      ", code: ", attr(my.df[[variable.name]], "value.labels")),
                ##      absolute.log.filename)
                my.df[[variable.name]][these.are.special] <- NA
                return(as.numeric(my.df[[variable.name]]))
            } else {
                ## only special recodes
                recodes <- special.recodes
            }
        }
        numeric <- attr(my.df[[variable.name]], "value.labels")
        strings <- names(numeric)[match(
            my.df[[variable.name]][these.are.special], numeric)]
        numeric.again <- car::recode(strings, recodes)
        my.df[[variable.name]][these.are.special] <- numeric.again
    }
    return(as.numeric(my.df[[variable.name]]))
}

factor.from.named.atomic <- function(my.df, variable.name, s=NULL,
          special.recodes=NULL){
    ## If s is given, it should be a list with an element "standard.recodes"
    ## which must a be string.

    ## if the variable is missing, return NA
    if(is.na(match(variable.name, names(my.df)))){
        return(rep(NA, times = nrow(my.df)))
    }
    ## If the variable is present, but only consist of missing data,
    ## then return NA
    if(length(which(is.na(my.df[[variable.name]]) == FALSE)) == 0){
        return(rep(NA, times = nrow(my.df)))
    }
    if(is.null(s) == FALSE){
        if(is.null(special.recodes) == FALSE && special.recodes != ""){
            ## both standard and special recodes
            recodes <- paste(s$standard.recodes, special.recodes, sep = ";")
        } else {
            ## only standard recodes
            recodes <- s$standard.recodes
        }
    } else {
        if(is.null(special.recodes) || special.recodes == ""){
            ## no recodes at all
            recodes <- NULL
        } else {
            ## only special recodes
            recodes <- special.recodes
        }
    }
    interpreted.values <- attr(my.df[[variable.name]], "value.labels")
    if(length(interpreted.values) == 0){
        ## eg, HV208 in Egypt DHS-III
        return(ifelse(my.df[[variable.name]] == 1, TRUE, FALSE))
    } else {
        if(is.null(recodes) == FALSE){
            return(car::recode(
var=names(interpreted.values)[match(my.df[[variable.name]], interpreted.values)],
recodes=recodes, as.factor=TRUE))
        } else {
            ## No recodes, but still interpret data using the value.lables
            ## Useful when 'other' should not be set to NA (e.g. religion)
            return(factor(names(interpreted.values)[match(my.df[[variable.name]],
                                                          interpreted.values)]))
        }
    }
}

logical.from.named.atomic <- function(...){
    ifelse(factor.from.named.atomic(...)=="TRUE", TRUE, FALSE)
}

logical.compound.from.named.atomic <- function(list.of.variable.names, my.df, ...) {
    ## If there were no matches, list.of.variable.names will be of length 0
    if(length(list.of.variable.names) == 0){
        return(rep(NA, nrow(my.df)))
    }
    ## Disregard variables which have only NA:s
    only.NA.here <- which(sapply(list.of.variable.names, function(x) {
        length(which(is.na(my.df[[x]]) == FALSE)) }) == 0)
    if(length(only.NA.here) > 0){
        some.actual.measurements <- list.of.variable.names[-only.NA.here]
    } else {
        some.actual.measurements <- list.of.variable.names
    }
    if(length(some.actual.measurements) > 0){
        compound <- sapply(some.actual.measurements, function(x) {
                           logical.from.named.atomic(my.df, x, ...) } )
        if(length(some.actual.measurements) == 1){
            return(as.vector(compound))
        } else {
            return(apply(compound, 1, any, na.rm=FALSE))
        }
    } else {
        ## All variables had ony NA:s
        return(rep(NA, nrow(my.df)))
    }
}

variable.packages.f <- function(variable.packages, file.type, data.set, s){
    
    list.of.data <- sapply(variable.packages, function(package){
        if(file.exists(system.file(paste(package, file.type, "csv", sep = "."), 
                                   package="globallivingconditions"))){
            ## There is a csv-file for this package and file-type
            meta.code <- read.csv(system.file(paste(package, file.type, "csv", 
                                sep = "."), package="globallivingconditions"))
            ## my.log.f(paste(Sys.time(), "found meta.code for", package, 
            ##          "for file type", file.type), absolute.log.filename)
## collect data for each row in the meta.code
## apply() forces the data to matrix which loses type information, 
## which is why we instead use sapply() with a counter here.
            data.as.list <- sapply(1:nrow(meta.code), function(i) {
                string.to.eval <- paste0(meta.code$type[i],
 ".from.named.atomic(my.df=data.set, '", meta.code$dhs.variable.name[i],
 "', s=s, special.recodes=", '"', meta.code$special.recodes.car[i], '"', ")")
 ## my.log.f(paste(Sys.time(), "evaluating this:", string.to.eval),
 ##          absolute.log.filename)
                my.result <- eval(parse(text=string.to.eval))
                if(length(which(is.na(my.result) == FALSE)) > 0){
    ##                 my.log.f(paste(Sys.time(), "found", 
    ## length(which(is.na(my.result) == FALSE)), "elements of non-missing data for",
    ## package, "for file type", file.type, "row number", i, "dhs variable name:", 
    ## meta.code$dhs.variable.name[i], "target variable name:", 
    ## meta.code$New.variable.name[i]), absolute.log.filename)
                    return(my.result)
                } else {
    ##                 my.log.f(paste(Sys.time(), "found only missing data for",
    ## package, "for file type", file.type, "row number", i, "dhs variable name:", 
    ## meta.code$dhs.variable.name[i], "target variable name:", 
    ## meta.code$New.variable.name[i]), absolute.log.filename)
                    ## return it anyway
                    return(my.result)
                }
              }, simplify=FALSE)
            data <- do.call(cbind, sapply(data.as.list, data.table, simplify=FALSE))
            colnames(data) <- paste(package, 
                             as.character(meta.code$New.variable.name), sep = ".")
            return(data)
        } else {
            return(NULL)
        }
    }, simplify=FALSE)
    ## as.data.frame(list.of.data[which(length(list.of.data) > 0)])
    return(Reduce(f=cbind, x=list.of.data))
}

wash.year.HV007 <- function(year, month, source, country, version){
    ## year and month are vectors, the other arguments are scalars.
    
    ## Deal with a vector of missing values.
    if(length(which(is.na(year))) == length(year)){
        ## All values are missing
        year <- rep(as.numeric(substring(sapply(strsplit(sapply(strsplit(
            as.character(source), "/"), function(x) { x[2] }), " "), 
            function(x) { x[2] }), 1, 4)), length(year))
    }
    
    ## 52 and 53 in Nepal (NPKR31FL.SAV) should be 96. They are 56 years ahead 
    ## of us 2052-56 = 1996
    ## 2062 and 2062 in Nepal (NP5) should be 2006 and 2007
    ## 2067 and 2068 in Nepal (NP6) should be 2011 and 2012

    ## Add 2000 to Nepal DHS III
    if(country == 524 & version == "31"){
        year <- year + 2000
    }

    ## Fix single and double digits
    ## India 1998-99 has 98, 99, and 0: => 1998, 1999, 2000; 
    ## Bangladesh 1999-00 has 99 and 0 => 1999, 2000; 
    ## Gabon 2000 has 0 and 1 => 2000, 2001
    ## 2 in Vietnam (vnkr41fl.sav)
        
    ## a single digit X means 200X
    these.have.one.digit <- which(nchar(year) == 1)
    if(length(these.have.one.digit) > 0){
        year[these.have.one.digit] <- year[these.have.one.digit] + 2000
    }

    ## two digits XY means 19XY
    these.have.two.digits <- which(nchar(year) == 2)
    if(length(these.have.two.digits) > 0){
        year[these.have.two.digits] <- year[these.have.two.digits] + 1900
    }

    ## Second, fix the calendar
    ## Nepal data sets, New years eve on 13-15 april, Month 1 starts in the middle
    ## of april. Subtract 56 or 57 years, depending on if month < 9
    if(country == 524){
        these <- which(month < 9)
        if(length(these) > 0){
            year[these] <- year[these] - 57
            year[-these] <- year[-these] - 56
        } else {
            year <- year - 56
        }
    }
    ## Add 12, or 13 to all Ethiopian datasets. New years eve is on 10th of September.
    ## Months start in september, for months 1:4, add 13, for higher months, add 12
    if(country == 231){
        these <- which(month < 5)
        if(length(these) > 0){
            year[these] <- year[these] + 9
            year[-these] <- year[-these] + 8
        } else {
            year <- year + 8
        }
    }
    
    ## Islamic calendar in Afghanistan
    ## Islamic year 1 is year 622 CE, the difference is 622-1 = 621
    if(country == 4){
        year <- year + 621
    }
    return(as.numeric(year))
}

wash.month.HV006 <- function(month, source, country, version){
    if(country %in% c(524, 231)){
        if(country == 524){
            these <- which(month < 9) ## The gregorian calender resets in month 8
            if(length(these) > 0){
                month[these] <- month[these] + 4
                month[-these] <- month[-these] - 8
            } else {
                month <- month - 8
            }
        }
        if(country == 231){
            these <- which(month < 5) ## The gregorian calender resets in month 5
            if(length(these) > 0){
                month[these] <- month[these] + 8
                month[-these] <- month[-these] - 6
            } else {
                month <- month - 6
            }
        }
    }
    return(month)
}

wash.pr.files <- function(pr.file.name, settings, absolute.log.filename, variable.packages) {
    library(data.table)
    if(file.exists(pr.file.name) == FALSE){
        return(NULL)
    }
    my.log.f(paste("Washing", pr.file.name), absolute.log.filename)
    ## Create a lock file that we remove on error-free exit.
    writeChar(object="a", con=paste0(pr.file.name, ".lock"))
    pr.df <- data.table::data.table(get(load(pr.file.name)))
    ## if there are duplicates, remove them now.
    if(length(which(is.na(pr.df$PersonID.unique) == FALSE)) > 0){
        dups <- which(duplicated(pr.df$PersonID.unique))
        if(length(dups) > 0){
            pr.df <- pr.df[match(unique(pr.df$PersonID.unique),
                                 pr.df$PersonID.unique), ]
        }
    }
    ## short alias
    s <- settings
    
    ## The primary source for Line number of mother is HV112, and
    ## the secondary source is HC60
    ## "HV112" might exist and be empty while HC60 actually has data
    if(is.na(match("HV112", names(pr.df))) == FALSE &&  
        length(which(is.na(pr.df$HV114) == FALSE)) > 0){
            use.this.var.for.mothers.line.number <- "HV112"
    } else {
        use.this.var.for.mothers.line.number <- "HC60"
    }
    
    ## support variable packages
    if(is.null(variable.packages) == FALSE){
        extra.data <- variable.packages.f(variable.packages, "PR", pr.df, s)
    }
    
    ## Add the filename as a variable
    source <- factor(pr.file.name)
    ## add the UN three digit country code. TODO make robust for new countries
    country.code.ISO.3166.alpha.3 <- country.codes.map$UN[
         match(substring(pr.df$HV000[1], 1, 2), country.codes.map$DHS)]
    
    month.of.interview.temp = numeric.from.named.atomic(pr.df, "HV006", s,
 special.recodes = "c('Chaitra', 'December') = 12; c('Falgun', 'November') = 11;
 c('Magh', 'October') = 10; c('September', 'Poush') = 9; 
c('Mangsir', 'August') = 8; c('Kartik', 'July') = 7; c('Aswin', 'June') = 6;
c('Bhadra', 'May') = 5; c('Srawan', 'April') = 4; c('Ashad', 'March') = 3;
c('Jestha', 'February') = 2; c('Baisakh', 'January') = 1; 97 = NA")
   year.of.interview = wash.year.HV007(year=pr.df$HV007, 
       month=month.of.interview.temp, source=source, 
       country=country.code.ISO.3166.alpha.3, version=pr.df$version[1])
   month.of.interview = wash.month.HV006(month=month.of.interview.temp,
       source=source, country=country.code.ISO.3166.alpha.3, 
       version=pr.df$version[1])

    if(source %in% c("DHS-II/India 1992-93/IAPR23FL.RData", 
                     "DHS-IV/India 1998-99/IAPR42FL.RData")){
        district = factor(add.missing.district.names(source, pr.df$RegionID,
                             pr.df[['District|Name.of.District.HH.Info']]))
    } else {
        district = factor.from.named.atomic(my.df=pr.df, 
                                      'District|Name.of.District.HH.Info', s)
    }

    washed.pr.df <- data.table(
        source,
        country.code.ISO.3166.alpha.3,
        district,
        sample.weight = as.numeric(pr.df$HV005)/1E6,
        line.number.mother = numeric.from.named.atomic(my.df=pr.df,
                           use.this.var.for.mothers.line.number),
        line.number.father = numeric.from.named.atomic(my.df=pr.df, "HV114"),
        relation.to.hh = car::recode(
                                  factor.from.named.atomic(my.df=pr.df, "HV101", s),
                                  recodes = s$harmonise.relation.to.hh.recodes),
        sex = factor.from.named.atomic(my.df=pr.df, "HV104", s),
        age = as.numeric(pr.df$HV105),
        severe.education.deprivation = logical.from.named.atomic(my.df=pr.df,
                    "HV106", s, special.recodes = s$severe.deprivation.recodes),
        education = factor.from.named.atomic(my.df=pr.df, "HV106", s, 
                                  special.recodes = s$education.factor.recodes),
        education.in.years = numeric.from.named.atomic(my.df=pr.df, "HV108", s,
                                special.recodes = s$HV108.recodes),
        native.language = factor.from.named.atomic(my.df=pr.df,
                                                   "native.language", s),
        year.of.interview,
        month.of.interview,
        water = factor.from.named.atomic(my.df=pr.df, "HV201", s),
        time.to.water = numeric.from.named.atomic(my.df=pr.df, "HV204", s,
                                special.recodes = s$hv204.recodes),
        rural = factor.from.named.atomic(my.df=pr.df, "HV025", s, "'rural' = 'Rural'; 'urban' = 'Urban'"),
        caste = factor.from.named.atomic(my.df=pr.df, 
                            "Recoded.Household.Caste|Castetribe|Type", s=NULL,
                            special.recodes = "'OBC' = 'Other Backward Class'"),
        toilet = factor.from.named.atomic(my.df=pr.df, "HV205", s),
        refridgerator = logical.from.named.atomic(my.df=pr.df, "HV209", s),        
        bicycle = logical.from.named.atomic(my.df=pr.df, "HV210", s),
        motorcycle = logical.from.named.atomic(my.df=pr.df, "HV211", s),
        car = logical.from.named.atomic(my.df=pr.df, "HV212", s),
        floor =  factor.from.named.atomic(my.df=pr.df, "HV213", s),
        wall = factor.from.named.atomic(my.df=pr.df, "HV214", s),
        roof = factor.from.named.atomic(my.df=pr.df, "HV215", s),
        shares.toilet = logical.from.named.atomic(my.df=pr.df, "HV225", s),
        location.of.toilet = factor.from.named.atomic(my.df=pr.df, "SH109A", s), 
       type.of.cooking.fuel = factor.from.named.atomic(my.df=pr.df, "HV226", s),
       place.to.wash.hands = factor.from.named.atomic(my.df=pr.df, "HV230A", s),
        nr.of.sleeping.rooms = numeric.from.named.atomic(
            my.df=pr.df, "HV216", s, special.recodes = "
c('Under a tree', 'under the tree /in the open air', 'None') = 0;
c('20+', '25 or more rooms') = 20
"),
        person.who.fetches.water = factor.from.named.atomic(
            my.df=pr.df, "HV236", s),
        water.made.safe = logical.from.named.atomic(
            my.df=pr.df, "HV237", s, "'Yes - Always/ Sometimes' = TRUE"),
        cooking.on.stove.or.open.fire = factor.from.named.atomic(
            my.df=pr.df, "HV239", s),
        animal.drawn.cart = logical.from.named.atomic(
            my.df=pr.df, "HV243C", s),
        boat.with.motor = logical.from.named.atomic(
            my.df=pr.df, "HV243D", s),
        owns.land = logical.from.named.atomic(my.df=pr.df, "HV244", s),
        hectares.of.land = numeric.from.named.atomic(my.df=pr.df, "HV245", s,
           special.recodes = "
c('< 0.1 hecatares', 'Less than 0.1 hectare', 'Less than 0.1 hectares', '< 1 hectare (see SH123)', 'Less than 1 hectare (see SH60H)', 'Less than 1 acre') = 0;
'6.1 HA or more (from Tareas)' = 10; 
c('38.4 or more', '37 or more', '38 or more hectares', '38 hectares +', '66.5 HA or more (from Manzanas)', '70+', '95 hectares or more', '95 or more', '95.0 hectares or more', 'More than 95', 'More than 95 acres !!', '95 or more ACRES', '190 hectares or more') = 50;
c('unknown', 'Unknown', 'Missing', 'Don\\'t know', 'Area given in other than hectares', 'Area givien in other than hectares') = NA"),
        owns.livestock = logical.from.named.atomic(
            my.df=pr.df, "HV246", s),
        has.bankaccount = logical.from.named.atomic(
            my.df=pr.df, "HV247", s),
        has.fan = logical.from.named.atomic(
            my.df=pr.df, "fan", s),
        has.table = logical.from.named.atomic(
            my.df=pr.df, "table", s),
        has.oven = logical.from.named.atomic(
            my.df=pr.df, "oven", s),
        phone = logical.compound.from.named.atomic(list.of.variable.names=
            grep("phone", names(pr.df), value=TRUE, ignore.case=TRUE),
            my.df=pr.df, s),
        has.computer = logical.compound.from.named.atomic(
            grep("computer", names(pr.df), value=TRUE, ignore.case=TRUE),
            my.df=pr.df, s, special.recodes = s$computer.recodes),
        has.video = logical.compound.from.named.atomic(
            grep("(DVD|video)", names(pr.df), value=TRUE, ignore.case=TRUE),
            my.df=pr.df, s),
        owns.radio = logical.from.named.atomic(my.df=pr.df, "HV207", s),
        owns.tv = logical.from.named.atomic(my.df=pr.df, "HV208", s),
        has.electricity = logical.from.named.atomic(my.df=pr.df,
                                                     "HV206", s),
        has.watch = logical.from.named.atomic(my.df=pr.df, "watch", s),
        Marital.status.HA60 = factor.from.named.atomic(my.df=pr.df, "HA60", s)
        )
    ## If additional variable.packages is activated, then add whatever 
    ## data they produced
    if(exists("extra.data") && is.null(extra.data) == FALSE && nrow(extra.data) > 0){
        washed.pr.df <- cbind(washed.pr.df, extra.data)
    }
    
    ## Calculate information deprivation.
    washed.pr.df$information.deprivation <- ! apply(washed.pr.df[, c("phone",
  "has.computer", "has.video", "owns.radio", "owns.tv")], MARGIN=1, FUN=any, 
  na.rm=TRUE)
    
    ## If the variable.packge `wealth` is requested, calculate iwi, using 
    ## functions in the iwi package
    if("wealth" %in% variable.packages){
        washed.pr.df$iwi <- iwi::iwi.calculator(cbind(washed.pr.df, pr.df[, c(
           "HV201", "HV205", "HV213", "HV206", "HV207", "HV208", "HV209", "HV210",
           "HV212", "HV216", "HV221", "HV243A")]))
    }

    ## HHID is missing in "UGPR6AFL.SAV", so only add variables that exist
    my.cols <- names(pr.df)[na.omit(match(c("HHID", s$general.identifiers), 
                                          names(pr.df)))]
    my.df <- cbind(subset(pr.df, , my.cols), washed.pr.df)
    save(my.df, file=paste(pr.file.name, "washed", sep = "."))
    my.log.f(paste(Sys.time(), "Washed", pr.file.name, "n.cases:", nrow(my.df)),
                                              absolute.log.filename)
    ## Remove the lock-file
    unlink(paste0(pr.file.name, ".lock"))
}

## any() returns FALSE on NA, if na.rm=TRUE
## and returns NA on c(NA, FALSE) if na.rm=FALSE
my.any <- function(log.vec, ...){
    if(length(which(is.na(log.vec) == FALSE)) == 0){
        return(NA)
    } else {
        return(any(log.vec, ...))
    }
}

immunization.f <- function(my.dt, s){
    ## return missing if all is missing
    polio <- c("H0", "H3", "H5", "H7")
    dpt <- c("H4", "H6", "H8")
    measles <- "H9"
    any.vacc <- "H10"
    ## H0, 3, 5, 7 is Polio (H0 at birth)
    ## H4, 6, 8 is difteri, tetanus and pertussis. (DTP)
    ## H9 is measles, 
    ## H10 is any immunization (if the vaccination card is absent)
    ## Note the manual says this variable (H10) is defined only children whose
    ## mother could not produce a health card, i.e. it is *not* a summary of
    ## H0-H9
    vars <- c(polio, dpt, measles, any.vacc)
    foo <- my.dt[, ..vars, ]    
    if(length(which(is.na(unlist(foo)))) == length(unlist(foo))){
        return(data.table(no.immunization = rep(NA, nrow(my.dt)), 
                          no.polio.vacc = rep(NA, nrow(my.dt)), 
                          no.dpt.vacc = rep(NA, nrow(my.dt)),
                          no.vacc.measles = rep(NA, nrow(my.dt))))
    } else {
        ## Return TRUE if any cell in the row is true.
     my.data <- sapply(vars, function(x) { logical.from.named.atomic(
                my.df=my.dt, variable.name=x, s, special.recodes = "
c('Vaccination marked on card', 'Reported by mother', 'Mother reported',
  'Vaccination date on card', 'Vacc. marked on card', 
'Vacc. date on card') = TRUE; '< 3 months' = NA") })
        ## any.immunization <- apply(my.data, 1, my.any, na.rm=TRUE)
        no.immunization <- !apply(my.data, 1, my.any, na.rm=TRUE)        
        ## any.polio.vacc <- apply(my.data[, polio], 1, my.any, na.rm=TRUE)
        no.polio.vacc <- !apply(my.data[, polio], 1, my.any, na.rm=TRUE)        
        ## any.dpt.vacc <- apply(my.data[, dpt], 1, my.any, na.rm=TRUE)
        no.dpt.vacc <- !apply(my.data[, dpt], 1, my.any, na.rm=TRUE)        
        ## vacc.measles <- my.data[, measles]
        no.vacc.measles <- !my.data[, measles]        
return(data.table(no.immunization, no.polio.vacc, no.dpt.vacc, no.vacc.measles))
    }
}

did.not.receive.treatment.f <- function(my.dt, s){
    ## return missing if all is missing.
    vars <- c("H22", "H31", "H31B", "H31C", "H11")
    foo <- my.dt[, ..vars, ]
    if(length(which(is.na(unlist(foo)))) == length(unlist(foo))){
        return(rep(NA, nrow(my.dt)))
    }
    
  Had.fever = ifelse(logical.from.named.atomic(my.df=my.dt, "H22", s), TRUE, NA)
    
    Had.cough.H31 = logical.from.named.atomic(my.df=my.dt, "H31",
                s, special.recodes = "c('Yes, last two weeks', 'Last two weeks',
'Yes, last 14 days', 'Yes, last 2-14 days', 'Yes, last 24 hours') = TRUE")

    Short.rapid.breaths.H31B = logical.from.named.atomic(my.df=my.dt,
                   variable.name = "H31B", s)

    Problem.chest.or.blocked.or.running.nose.H31C = 
        logical.from.named.atomic(
        my.df=my.dt, "H31C", s=NULL, special.recodes = 
     "c('Both', 'Nose only', 'Chest only') = TRUE; c('other', 'Other') = FALSE")
    
    illness <- ifelse(is.na(Had.fever & (Had.cough.H31 | Short.rapid.breaths.H31B
                  | Problem.chest.or.blocked.or.running.nose.H31C)), FALSE, TRUE)
    
    Medical.treatment.for.fever.cough.H32Z = { tmp <- factor.from.named.atomic(
                                                   my.df=my.dt, "H32Z", s);
        if(class(tmp) == "logical"){
            ## Must have been only NA:s
            tmp
        } else {
            dplyr::recode(tmp, 'Yes: medical treatm.' = TRUE,
                               'Yes: medical treatment' = TRUE,
                               'Yes, medical treatment' = TRUE,
                               'Yes' = TRUE,
                               "FALSE" = FALSE, "TRUE" = TRUE)
        }
    }
    ##, special.recodes = "NA = FALSE"),
    ## ":" is a special character in car::recode(), so when value labels that 
    ## need recoding include ":" we can't use car::recode()
  untreated.fever.cough.H32Y = { tmp <- factor.from.named.atomic(my.dt, "H32Y",
                                                                            s); 
        if(class(tmp) == "logical"){
            ## Must have been only NA:s
            tmp
        } else {
            dplyr::recode(tmp, 'No: received treatment' = FALSE,
                               'No: received treatm.' = FALSE,
                               'No: received treatm' = FALSE,
                               'Yes: no treatment' = TRUE,
                                 "FALSE" = FALSE, "TRUE" = TRUE)
        }
    }
    
    Had.Diarrhea.last.two.weeks.H11 = logical.from.named.atomic(
      my.df=my.dt, "H11", s, special.recodes = 
                     "c('Yes, last 2-14 days', 'Yes, last two weeks', 
                        'Yes, Last 2 Weeks', 'Yes, last 24 hours') = TRUE")
    ## ":" is a special character in car::recode(), so when value labels that 
    ## need recoding include ":" we can't use car::recode(). dplyr::recode()
    ## can't handle totally missing data well, so test for that before sending 
    ## the data to dplyr::recode()

    Treated.for.diarrhea = {tmp <- factor.from.named.atomic(my.dt, "H12Y", s);
        if(class(tmp) == "logical"){
            ## Must have been only NA:s
            tmp
        } else {
            dplyr::recode(tmp,   'No: received treatment' = FALSE,
                                 'No: received treatm.' = FALSE,
                                 'Yes: no treatment' = TRUE,
                                 "FALSE" = FALSE, "TRUE" = TRUE)
           }}

Received.sugar.salt.water.H13 = {tmp<-factor.from.named.atomic(my.dt, "H13", s);
        if(class(tmp) == "logical"){
            ## Must have been only NA:s
            tmp
        } else {
   dplyr::recode(tmp, "Yes: ORS - probed" = TRUE, "Yes: ORS - spontan." = TRUE,
               "Yes: ORS - spontaneous" = TRUE, "Yes: ORS - spontaneously" = TRUE,
               "Yes: ORS  (packet, homemade S/S solution or homemade liquids" = TRUE,
               "FALSE" = FALSE, "TRUE" = TRUE)
        }}
    
    untreated.diarroea <- 
        ## Had Diarrhea
        Had.Diarrhea.last.two.weeks.H11 & 
        ## Was not treated
     (apply(cbind(Treated.for.diarrhea, Received.sugar.salt.water.H13), 
            1, my.any, na.rm=TRUE) == FALSE)

    did.not.receive.treatment <- apply(cbind(untreated.fever.cough.H32Y, 
                                  untreated.diarroea), 1, my.any, na.rm=TRUE)
    return(did.not.receive.treatment)
}

wash.kr.files <- function(kr.file.name, settings, variable.packages) {
    if(file.exists(kr.file.name) == FALSE){
        return(NULL)
    }
    my.log.f(paste(Sys.time(), "Washing", kr.file.name))
    ## Create a lock file that we remove on error-free exit.
    writeChar(object="a", con=paste0(kr.file.name, ".lock"))
    kr.df <- data.table::data.table(get(load(kr.file.name)))
    if(length(which(is.na(kr.df$PersonID.unique) == FALSE)) > 0){    
        ## if there are duplicates, remove them now.
        dups <- which(duplicated(kr.df$PersonID.unique))
        if(length(dups) > 0){
            kr.df <- kr.df[match(unique(kr.df$PersonID.unique), 
                                 kr.df$PersonID.unique), ]
        }
    }
    ## short alias
    s <- settings
    hwx.recodes <- 'c("Other", "Refused", "Not present") = NA'
    if(length(grep("V016", names(kr.df))) > 0){
        day.of.interview <- kr.df$V016
    } else {
        day.of.interview <- rep(NA, times = nrow(kr.df))
    }
    
    ## support variable packages
    if(is.null(variable.packages) == FALSE){
        extra.data <- variable.packages.f(variable.packages, "KR", kr.df, s)
    }
    
    immunization <- immunization.f(kr.df, s)

    washed.kr.df <- data.table(
        day.of.interview,
        Currently.breastfeeding = logical.from.named.atomic(my.df=kr.df,
                               variable.name = "V404", s),
        Months.of.breastfeeding = numeric.from.named.atomic(my.df=kr.df,
                              variable.name = "M5", s,
        special.recodes = "c('Never breastfed',
                     'Ever breastfed, not currently breastfeeding') = NA"),

       do.call(cbind.data.frame, sapply(paste("HW", 1:3, sep = ""),
          function(x) { numeric.from.named.atomic(my.df=kr.df,
                    variable.name = x, s = NULL, special.recodes = hwx.recodes)
                 }, simplify=FALSE)),

       did.not.receive.treatment = did.not.receive.treatment.f(kr.df, s))

   washed.kr.df <- cbind(washed.kr.df, immunization)

    ## If additional variable.packages is activated, then add whatever 
    ## data they produced.
    if(exists("extra.data") && is.null(extra.data) == FALSE && nrow(extra.data) > 0){
        washed.kr.df <- cbind(washed.kr.df, extra.data)
    }

    ## Stuff we need for identification but don't need to wash: B4, B8, V003
    my.cols <- names(kr.df)[na.omit(match(
               c("B4", "B8", "V003", s$general.identifiers), names(kr.df)))]
    my.df <- cbind(subset(kr.df, , my.cols), washed.kr.df)

    save(my.df, file=paste(kr.file.name, "washed", sep = "."))
    my.log.f(paste(Sys.time(), "Washed", kr.file.name, "n.cases:", nrow(my.df)))
    ## Remove the lock-file
    unlink(paste0(kr.file.name, ".lock"))
}

