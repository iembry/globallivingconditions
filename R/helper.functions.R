variable.packages.names.f <- function(variable.packages, file.type, name.type){
    list.of.data <- sapply(variable.packages, function(package){
        if(file.exists(system.file(paste(package, file.type, "csv", sep = "."), 
                                   package="globallivingconditions"))){
            ## There is a csv-file for this package and file-type
            meta.code <- read.csv(system.file(paste(package, file.type, "csv", 
                                sep = "."), package="globallivingconditions"))
            ## collect data for each row in the meta.code
            data <- apply(meta.code, 1, function(x) {
                x[[name.type]]
              })
            return(data)
        } else {
            return(NULL)
        }
    })
    variable.names <- as.vector(unlist(list.of.data))
##     if(length(variable.names) > 0){
##      my.log.f(paste0(Sys.time(), " variable.package '", paste(variable.packages,
## collapse=", "), "' for file type ", file.type, " wants: ", paste(variable.names, 
##                                       collapse=", ")), absolute.log.filename)
##      }
    return(variable.names)
}

calc.observations.inner <- function(list.of.values, cat.dt, variable){
    tab <- table(cat.dt[[variable]])
    observed <- list.of.values[which(is.na(match(
        list.of.values, names(tab))) == FALSE)]
    my.df <- data.frame(type = observed,
                        n = as.numeric(tab[match(observed, names(tab))]))
    this.order <- order(trimws(gsub("[(| |¹|.]", "", my.df$type)))
    return(my.df[this.order,])
}

calc.observations.outer <- function(variable, vector.of.lists.of.values){
    load("cat.dt.RData")
    foo <- sapply(vector.of.lists.of.values, calc.observations.inner, cat.dt = cat.dt, variable = variable, simplify = FALSE)
}

my.classify.f <- function(rules, labels, verbose=FALSE){
    ## firstly, combine objects and properties
    these.have.both.property.and.object.full <-
        sapply(rules$properties, function(property) {
            these.have.property <- grep(property, labels, ignore.case = TRUE, perl=TRUE)
            these.have.object <- unique(unlist(sapply(rules$object, function(object) {
                grep(object, labels, ignore.case = TRUE)
            })))
            intersect(these.have.property, these.have.object)
        }, simplify=FALSE)
    ## secondly, search for the sufficient search term
    these.match.the.sufficient.search.term.full <- sapply(rules$sufficient,
        function(search.term) { grep(search.term, labels, ignore.case = TRUE, perl=TRUE) })
    ## thirdly, search for the exlude term
    these.match.the.exclude.search.term.full <- sapply(rules$exclude,
        function(search.term) { grep(search.term, labels, ignore.case = TRUE, perl=TRUE) })
    ## fourthly, exact search for the exact term
    these.match.the.exact.search.term.full <- sapply(rules$exact,
        function(search.term) { which(is.na(match(tolower(labels),
                                                  tolower(search.term))) == FALSE)
    })
    if(verbose){
        return(Hmisc::llist(these.have.both.property.and.object.full,
                     these.match.the.sufficient.search.term.full,
                     these.match.the.exclude.search.term.full,
                     these.match.the.exact.search.term.full))
    } else {
        return(as.numeric(c(
            setdiff(c(unlist(these.have.both.property.and.object.full),
                      unlist(these.match.the.sufficient.search.term.full)),
                    unlist(these.match.the.exclude.search.term.full)),
            unlist(these.match.the.exact.search.term.full))
            ))
    }
}

general.merger.f <- function(slim.files, pattern){
    files <- paste0(gsub("RData", "slim.dt", slim.files), pattern)
    my.log.f(paste(Sys.time(), "Merging slim files with pattern",
                   pattern, "first file is", files[1]))

    my.error <- try(my.data.slice <- data.table::rbindlist(foreach::foreach(
                                    file = files,
                                    .options.future = list(scheduling=FALSE)
    ) %dopar% {
                                       if(file.exists(file)){
                                           return(get(load(file)))
                                       } else {
                                           return(NULL)
                                       }
    },
        ## HV009 is missing in DHS-III/Senegal 1997, so add missing variables
        fill=TRUE
    ))
    if(length(attr(my.error, which = "condition")) > 0){
        my.log.f(paste(Sys.time(), "data.table / foreach failed with",
                       "this error", attr(my.error, which = "condition")))
    }
    save(my.data.slice, file = paste0("slice", pattern))
}

setup.future.f <- function(){
    if(.Platform$OS.type == "unix") {
        n.cores <- parallel::detectCores()
        options(mc.cores = n.cores)
        future::plan(future::multicore)
    } else {
        ## Use only max 8 cores on Windows, since Windows cannot fork
        n.cores <- min(16, parallel::detectCores())
        future::plan(future::multisession, gc=TRUE, workers=n.cores)    
    }
    doFuture::registerDoFuture()
    return(n.cores)
}
