download.and.harmonise <- function(dhs.user, dhs.password = getPass::getPass("Please enter your DHS password:"), mics.user, mics.password = getPass::getPass("Please enter your MICS password:"), log.filename="living-conditions.log", reporters = FALSE,
                                   vars.to.keep = c("m49.region", "country.code.ISO.3166.alpha.3", "version", "RegionID",
  "superClusterID", "ClusterID", "HouseholdID", "year.of.interview", "month.of.interview", "severe.education.deprivation",
  "severe.food.deprivation", "severe.water.deprivation", "severe.sanitation.deprivation", "severe.shelter.deprivation",
  "has.electricity", "age", "sex", "sample.weight", "lon", "lat"),
variables.to.inherit = c("severe.education.deprivation", "education",
"education.in.years", "Religion", "Age.at.first.cohabitation", "caste"),
  countries = NULL,
  waves = NULL,
  file.types.to.download = c("PR", "GE", "IR", "KR", "MR"),
  max.file.size.for.parallelisation = NULL,
  make.pdf = FALSE,
  directory = "global-living-conditions",
  variable.packages = NULL,
  satellite.images = FALSE,
  precipitation = FALSE,
  temperature = FALSE,
  malaria = FALSE,
  superclusters = TRUE,
  check.dhs.for.more.data = TRUE,
  qog.vars = c("wbgi_gee", "wdi_gnicapatlcur"),
  temperature.file.path = NULL,
  qog.file.path = NULL,
  use.renv = FALSE
    ){
    ## save the current working directory as an absolute path, so we can return.
    starting.dir <- getwd()

    ## also create a separate file tree for derived files in the current dir, if such a directory not already exists.
    ## since starting dir is an absolute path, r.dir is also an absolute.path
    r.dir <- paste(starting.dir, "globallivingconditions_cached_R_files", sep = "/")
    
    if(utils::file_test("-d", r.dir) == FALSE){
        dir.create(r.dir)
    }
        
    # save final.RData
    save(r.dir, file = paste(r.dir, "final.RData", sep = "/"))
    
    ## If the directory for raw data files does not exist, then create it
    if(utils::file_test("-d", directory) == FALSE){
        dir.create(directory)
    }
    
    ## if `directory` is a relative path, make it into a absolute path now that we can return to
    setwd(directory)
    directory <- getwd()

    ## return back to starting.dir
    setwd(starting.dir)

    ## Actually, try running in r.dir
    setwd(r.dir)

    ## Set some options for knitr()
    knitr::opts_chunk$set(results='hide', cache=TRUE, echo=TRUE, warning=TRUE, fig.pos = 'htb', 
                   tidy.opts=list(blank=FALSE, width.cutoff=50), background='white',
                   tidy=FALSE, error=TRUE, message=FALSE, dev="pdf", autodep = TRUE)
    knitr::opts_knit$set(root.dir = ".")
    ## knitr::opts_knit$set(root.dir = my.cache.path)
    options(scipen=999) ## Avoid scientific formating of large numbers.

    ## variables in this environment (log.filename, dhs.user, etc) will be available in knit() as well.
    knitr::set_parent(system.file("latex-headers.Rnw", package="globallivingconditions"))
    if(make.pdf){
        knitr::knit2pdf(system.file("harmonise-and-merge.Rnw", package="globallivingconditions"),
                    compiler="xelatex")
    } else {
        knitr::knit(system.file("harmonise-and-merge.Rnw", package="globallivingconditions"))
    }
    
    ## be a nice citizen, 2. return to the original working dir
    setwd(starting.dir)
    return(get(load(paste(r.dir, "final.RData", sep = "/"))))
}
