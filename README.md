# README #

## What is this repository for? ##

With this R package, I want to help researchers by providing harmonised surveydata useful for analysis of poverty, development, and more generally, global living conditions.

1.0.0 Release date 2019-02-22

## Introduction

I'm in a hurry, what are the instructions for a minimal data set?

This minimal data set will consist of **all available countries** for each user.

    ## Check to see if devtools is already installed. If not, then install devtools.
    if (!requireNamespace("devtools")) {
    install.packages("devtools")
    }
    
    library(devtools)
    install_github(repo = "JanMarvin/readspss")
    install_bitbucket(repo = "hansekbrand/iwi")
    install_bitbucket(repo = "hansekbrand/DHSharmonisation")
    
    library(globallivingconditions)
    dt <- download.and.harmonise(dhs.user="", # your username at DHS goes here
                           dhs.password = getPass::getPass("Please enter your DHS password:"), # your password at DHS goes here
                           mics.user="", # your username at MICS goes here
                           mics.password = getPass::getPass("Please enter your MICS password:"), # your password at MICS goes here
                           reporters = FALSE, # the Reports will not be downloaded for each country (default) 
                           log.filename="living-conditions.log",
                           use.renv = FALSE # FALSE (the default) means the R library for this project will not be isolated
                           )

`dt` will now have a minimal data set without external sources. Note that if you have access to many countries `download.and.harmonise()` will take many hours (Even a single country can take an hour).


# Help

With credit due to [Getting Help with R](https://www.r-project.org/help.html) for the following methods of help:

```R
help(remove_mics_temp_merged_folders, package = "globallivingconditions")

# Remove Unnecessary Folders and Files function help
```



```R
help(mics_runs, package = "globallivingconditions")

# Re-Run the MICS Merging and Harmonizing for a MICS Round function help
```

Please refer to the function help prior to removing any folders and/or re-running the MICS analysis for any rounds.



## After successful completion of the MICS and DHS harmonization, you can remove the unnecessary MICS folders (Temporary and MICS_Merged) with the following function
    
If you are currently in the starting.dir (original working directory) for this analysis, you can copy-and-paste the following into R [Please refer to the help documentation first]:

    remove_mics_temp_merged_folders(mics_temp_folders_keep = FALSE, # FALSE (the default) means remove the temporary folders 
                           mics_merged_folders_keep = FALSE, # FALSE (the default) means remove the merged folder
                           mics_rounds_folder_keep = TRUE, # TRUE (the default) means keep the rounds folder
                           log.filename = "living-conditions.log" # the filename should be the same as used in the download.and.harmonise function call
                           )
                           

If you are NOT currently in the starting.dir (original working directory) for this analysis, you can copy-and-paste the following into R [Please refer to the help documentation first]:

    remove_mics_temp_merged_folders(mics_temp_folders_keep = FALSE, # FALSE (the default) means remove the temporary folders 
                           mics_merged_folders_keep = FALSE, # FALSE (the default) means remove the merged folder
                           mics_rounds_folder_keep = TRUE, # TRUE (the default) means keep the rounds folder
                           starting.dir = "full path name of the original working directory", 
                           log.filename = "living-conditions.log" # the filename should be the same as used in the download.and.harmonise function call
                           )
           


Read on if you need more than that.

## How to install the package "globallivingconditions" ##

Until this package is available on CRAN, I recommend install the package with `devtools`:

  A.    Install `devtools` if you do not already have it installed.

Note that if your OS *compiles* R packages (rather than copying ready-made binaries from the internet), i.e. if you are on OS X or Linux, then to build `devtools`, development files for `curl` are necessary, and supplied in Debian GNU/Linux by the package `libcurl4-gnutls-dev`. `devtools` also depends on `openssl` which requires development files included in `libssl-dev`. `globallivingconditions` depends on `XML` which has `libxml2-dev` as a build dependency, and `rgdal` which in turn depends on `libgdal-dev` and `libproj-dev`. For Debian GNU/Linux the following command will make sure that the dependencies are satisfied:

    apt-get install libcurl4-gnutls-dev libssl-dev libxml2-dev libgdal-dev libproj-dev 

### Also, MPI needs to be included too
### on linux, libopenmpi-dev is a build dependency for the next line of R code
### on MS Windows, Microsoft MPI is a build dependency for the next line of R code
### on Mac OS, OpenMPI is a build dependency for the next line of R code
    
Issue this within R

    install.packages("devtools")
    library(devtools)

  B.    Use `devtools` to install "globallivingconditions"

Issue this within R

    install_github(repo = "JanMarvin/readspss")
    install_bitbucket(repo = "hansekbrand/iwi")
    install_bitbucket(repo = "hansekbrand/DHSharmonisation")

This will automatically install all dependencies, recursively.

## Deployment instructions ##

I have found it convenient to have my credentials stored in a file which can be loaded by many different scripts. It also makes it possible to share scripts with others without sharing your secret credential.

    credentials <- list(dhs.user = "foo@bar.com", dhs.password = "r3al.password", mics.user = "foos@bar.com", mics.password = "Hr9nqE.passwords")
    save(credentials, file = paste0(normalizePath("~"), "user_credentials_dhs.RData", sep = "/"))
    
This way, the credentials can be loaded regardless of the current directory

    library(globallivingconditions)
    load("user_credentials_dhs.RData") # a list with four named elements:
                                       # "dhs.user", "dhs.password", "mics.user", and "mics.password"
    my.dt <- download.and.harmonise(
        dhs.user=credentials$dhs.user,
        dhs.password=credentials$dhs.password,
        mics.user=credentials$mics.user,
        mics.password=credentials$mics.password,
        iwi.path = paste0(my.data.dir, "iwi"),
        drought.data.path = paste0(my.data.dir, "vcpct--VIC_DERIVED"),                                
        flooding.data.path = paste0(my.data.dir, "runoff--VIC_3B42RT"),
        armed.conflict.filename = paste0(
            my.data.dir,
            "Armed-Conflict/intensity-of-armed-conflicts-Africa-UCDP.RData"),
        countries = c("Nigeria"),
        reporters = FALSE,
        log.filename = "living-conditions.log",
        use.renv = FALSE
    )
    
    
## After successful completion of the MICS and DHS harmonization, you can remove the unnecessary MICS folders (Temporary and MICS_Merged) with the following function
    
If you are currently in the starting.dir (original working directory) for this analysis, you can copy-and-paste the following into R [Please refer to the help documentation first]:

    remove_mics_temp_merged_folders(mics_temp_folders_keep = FALSE, # FALSE (the default) means remove the temporary folders 
                           mics_merged_folders_keep = FALSE, # FALSE (the default) means remove the merged folder
                           mics_rounds_folder_keep = TRUE, # TRUE (the default) means keep the rounds folder
                           log.filename = "living-conditions.log" # the filename should be the same as used in the download.and.harmonise function call
                           )
                           
                           

If you are NOT currently in the starting.dir (original working directory) for this analysis, you can copy-and-paste the following into R [Please refer to the help documentation first]:

    remove_mics_temp_merged_folders(mics_temp_folders_keep = FALSE, # FALSE (the default) means remove the temporary folders 
                           mics_merged_folders_keep = FALSE, # FALSE (the default) means remove the merged folder
                           mics_rounds_folder_keep = TRUE, # TRUE (the default) means keep the rounds folder
                           starting.dir = "full path name of the original working directory", 
                           log.filename = "living-conditions.log" # the filename should be the same as used in the download.and.harmonise function call
                           )
           


To get a minimal data set, without any external data sources, simply exclude the path arguments, since they default to NULL.

    library(globallivingconditions)
    load("user_credentials_dhs.RData") # a list with four named elements:
                                       # "dhs.user", "dhs.password", "mics.user", and "mics.password" 
    download.and.harmonise(dhs.user=credentials$dhs.user,
                           dhs.password=credentials$dhs.password,
                           mics.user=credentials$mics.user,
                           mics.password=credentials$mics.password,
                           reporters = FALSE,
                           log.filename="living-conditions.log",
                           use.renv = FALSE)

                           
    # After successful completion of the MICS and DHS harmonization, you can remove the unnecessary MICS folders (Temporary and MICS_Merged) with the following function:
    
If you are currently in the starting.dir (original working directory) for this analysis, you can copy-and-paste the following into R [Please refer to the help documentation first]:

    remove_mics_temp_merged_folders(mics_temp_folders_keep = FALSE, # FALSE (the default) means remove the temporary folders 
                           mics_merged_folders_keep = FALSE, # FALSE (the default) means remove the merged folder
                           mics_rounds_folder_keep = TRUE, # TRUE (the default) means keep the rounds folder
                           log.filename = "living-conditions.log" # the filename should be the same as used in the download.and.harmonise function call
                           )
                           
                           

If you are NOT currently in the starting.dir (original working directory) for this analysis, you can copy-and-paste the following into R [Please refer to the help documentation first]:

    remove_mics_temp_merged_folders(mics_temp_folders_keep = FALSE, # FALSE (the default) means remove the temporary folders 
                           mics_merged_folders_keep = FALSE, # FALSE (the default) means remove the merged folder
                           mics_rounds_folder_keep = TRUE, # TRUE (the default) means keep the rounds folder
                           starting.dir = "full path name of the original working directory",
                           log.filename = "living-conditions.log" # the filename should be the same as used in the download.and.harmonise function call
                           )

                           
## Alternatively, if you wish to have the tryCatchLog messages (which are used in the MICS analysis) saved to a .log file rather than printed to the R console, you can copy-and-paste the following into R

    ## Check to see if devtools is already installed. If not, then install devtools.
    if (!requireNamespace("devtools")) {
    install.packages("devtools")
    }
    
    library(devtools)
    install_github(repo = "JanMarvin/readspss")
    install_bitbucket(repo = "hansekbrand/iwi")
    install_bitbucket(repo = "hansekbrand/DHSharmonisation")
    
    ## Check to see if futile.logger is already installed. If not, then install futile.logger.
    if (!requireNamespace("futile.logger")) {
    install.packages("futile.logger")
    }
    
    ## Check to see if tryCatchLog is already installed. If not, then install tryCatchLog.
    if (!requireNamespace("tryCatchLog")) {
    install.packages("tryCatchLog")
    }
    
    library(futile.logger)
    library(tryCatchLog)
    library(globallivingconditions)

    # Source: https://cran.r-project.org/web//packages//tryCatchLog/vignettes/tryCatchLog-intro.html
    # Error handling in R with tryCatchLog: Catching, logging, post-mortem analysis
    # Jürgen Altfeld
    # 2021-10-24
    
    options(keep.source = TRUE) # source code file name and line number tracking
    options("tryCatchLog.write.error.dump.file" = FALSE) # if TRUE, create dump files for post-mortem analysis
    flog.appender(appender.file("mics.log")) # to log into a file instead of console -- feel free to select a different name
    flog.threshold(WARN) # WARN, ERROR, FATAL


### The following will get you only a minimal data set, but you can modify the call to `download.and.harmonise` as you please:

    tryCatchLog(my.dt <- download.and.harmonise(dhs.user="", # your username at DHS goes here
                           dhs.password = getPass::getPass("Please enter your DHS password:"), # your password at DHS goes here
                           mics.user="", # your username at MICS goes here
                           mics.password = getPass::getPass("Please enter your MICS password:"), # your password at MICS goes here
                           reporters = FALSE, # the Reports will not be downloaded for each country (default) 
                           log.filename="living-conditions.log",
                           use.renv = FALSE # FALSE (the default) means the R library for this project will not be isolated
                           ))


                           

## FAQ ##

### Why is this package written in form of a knitr-document? ###

The end user will not notice that knitr is involved under the hood,
the end user will only see a function that given username and password
to the DHS-server and MICS-server, will return the dataset.

Perhaps we won't need knitr when we are done, but
during development, I think it is a good choice, for three reasons:

#### 1. The ability to cache work done during development. ####

We are dealing with 23 GB of data which, in many steps is refined into
a dataset of about 11.5 million rows and 130 MB of data. For clarity
this process is divided into blocks that is small enough to
comprehend. The most computationally efficient way would be to only
load one piece of data once, and do all manipulations on it right
away. However, for a human to grasp all the manipulations,
partitioning the task into small bits is much easier to understand,
modify, and debug.

knitr automates parts of the caching needed for that workflow. The low
RAM requirement (which came later in the process, long after I had
settled on knitr), reduced the strength of the caching part somewhat,
since I use external files for more and more of the caching.


#### 2. To minimise the work required to keep documentation and code in sync ####

To minimise the work required to keep documentation and code in sync,
i.e. for the same reason that knitr, or, more generally, literate
programming (before there was knitr I used pgfSweave), is a good
choice for replicable research.


#### 3. The output of that document is useful for validation ####

Also, the harmonisation program is rather big, about 2200 lines of
code. If instead of knitr-document I would have made one master
function that controlled the complete process, then I would have to
explain what it did, and the kind of objects it deals with and
creates, by means of comments. With knitr, I can easily show the data
by creating a table or a graph. All tables and graphs in the PDF are
strictly speaking not necessary to achieve the harmonisation, but they
do a good job explaining what is happening, and they are essential to
validation - we can immediately tell that this categorisation of a type
of floor is correctly or incorrectly classified.

## Who do I talk to? ##

> Hans Ekbrand <hans.ekbrand@gu.se>
> University of Gothenburg, department of sociology and work science
> Sweden
