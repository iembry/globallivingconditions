\encoding{UTF-8}
\name{remove_mics_temp_merged_folders}
\alias{remove_mics_temp_merged_folders}
\title{Remove Unnecessary Folders and Files}
\source{
\enumerate{
   \item Add 'y to confirm' confirmation dialogue to an R function? - Stack Overflow answered by Ronak Shah on Nov 12, 2020. See \url{https://stackoverflow.com/questions/64802506/add-y-to-confirm-confirmation-dialogue-to-an-r-function}.
   \item Writing R Extensions 2.1.1 Documenting functions examples subsection -- R, version 4.2.3 (2023-03-15). See \url{https://cran.r-project.org/doc/manuals/r-release/R-exts.html#Writing-R-documentation-files}.
}
}
\usage{
remove_mics_temp_merged_folders(
  mics_temp_folders_keep = FALSE,
  mics_merged_folder_keep = FALSE,
  mics_rounds_folder_keep = TRUE,
  starting.dir = NULL,
  log.filename = "living-conditions.log"
)
}
\arguments{
\item{mics_temp_folders_keep}{logical vector. Default is FALSE (remove the
MICS Temporary folders). TRUE refers to keep the folders.}

\item{mics_merged_folder_keep}{logical vector. Default is FALSE (remove the
MICS_Merged folder). TRUE refers to keep the folder.}

\item{mics_rounds_folder_keep}{logical vector. Default is TRUE (keep the
folder). FALSE refers to remove the MICS_Rounds folder.}

\item{starting.dir}{character vector containing the full path name for the
final.RData file or either the full path name for the starting directory,
which is the location of the globallivingconditions_cached_R_files
top-level directory (the MICS_Rounds, MICS_Merged, MICS_Harmonized,
MICS_Dictionary, all country folders, etc. are subdirectories of that
top-level folder) folder that the user user wishes to remove the MICS
Temporary, MICS_Merged, and/or MICS_Rounds folders from. If missing, it
will be assumed that the user's current working directory is the correct
location to remove the folders from. The user's current working directory
should be the location of the globallivingconditions_cached_R_files folder
(e.g., /home/user/surveys/globallivingconditions_cached_R_files/) that the
user wishes to remove folders from.}

\item{log.filename}{character vector containing the previously defined log
filename. If the user wishes to declare a different log filename, please
include the filename with the extension of \emph{.log} here. The default
is "living-conditions.log".}
}
\value{
if folders are deleted, a note will be appended to the log file. If
   not, all folders will be kept.
}
\description{
This function deletes unnecessary files and folders after completion of the
MICS analysis.
}
\note{
Please note: The Temporary folders exist within each round (2 - 6) of the
MICS_Merged folder and in each round (2 - 6) of the MICS_Harmonized folder.
All of the Temporary folders hold data during the intermediate steps for
both the merging and the harmonization processes.

The MICS_Merged folder contains all files needed in the intermediate steps
leading up to the actual harmonization of the variables and the deprivation
analysis.

As long as the MICS harmonization process has completed successfully, the
user can safely remove the MICS_Merged folder and all of its contents.

The MICS_Rounds folder and its contents are needed for proper execution of
the \code{\link{mics_runs}} function.

If the user wishes to re-run the merging and harmonization process for a
particular MICS round (or for all MICS rounds) that is possible via the
\code{\link{mics_runs}} function. Thus, even if the MICS_Merged folder or all of
the Temporary folders were accidentally removed, all of them can be re-
created. And the merging, harmonization, and deprivation analyses can be re-
run.
}
\examples{

# Examples

\dontrun{
# See Source 2

# In this example, the user supplies the .RData file
# Modify the full path name for your operating system, if needed
# The MICS Temporary folders will be kept
# The MICS_Merged folder and all of its contents will be removed
# The MICS_Rounds folder and all of its contents will be kept
# The log.filename has been changed

library(globallivingconditions)
try(remove_mics_temp_merged_folders(mics_temp_folders_keep = TRUE,
mics_merged_folder_keep = FALSE, mics_rounds_folder_keep = TRUE, starting.dir
= "/home/user/surveys/globallivingconditions_cached_R_files/final.RData",
log.filename = "living_conditions.log"))



# In this example, the user supplies the path name to the working directory.
# Modify the full path name for your operating system, if needed
# The MICS Temporary folders will be kept
# The MICS_Merged folder and all of its contents will be removed
# The MICS_Rounds folder and all of its contents will be kept
# The log.filename has been changed

library(globallivingconditions)
try(remove_mics_temp_merged_folders(mics_temp_folders_keep = TRUE,
mics_merged_folder_keep = FALSE, mics_rounds_folder_keep = TRUE, starting.dir
= "/home/user/surveys/globallivingconditions_cached_R_files/",
log.filename = "livingconditions.log"))
}


}
\author{
Irucka Embry
}
