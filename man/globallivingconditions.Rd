\docType{package}
\name{globallivingconditions}
\alias{globallivingconditions}
\title{globallivingconditions: Collect and harmonise data from Demographics and Health Surveys and MICS}
\description{
globallivingconditions provides methods to collect and harmonise large
amounts of data from Demographics and Health Surveys and MICS survey
programmes. The package automates downloading of data (this part requires
the user to provide username and password for these programmes) and
harmonising parts of the data to one large dataset. By default, seven
indicators for child deprivation are derived using definitions
operationalised by Dave Gordon and Shailen Nandy, and a set of important
predictors for living conditions, coordinate data, International Wealth
Index. In addition, the data is prepared for comparative longitudinal
analysis, by harmonising variables describing the sample structure (e.g.
names of Regions within countries). The user can extend the package by
adding code to harmonise and derive other measures.
}
